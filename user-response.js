'use strict';

var GettingStarted = require('./actions/getting-started');

function UserResponse(event) {
  this.senderId = event.sender.id;
  this.message = event.message;
  this.text = event.message.text;
}

UserResponse.prototype.listen = function() {
  switch (this.text) {
    case 'Hi':
    case 'hi':
    case 'Hello':
    case 'hello':
      var gettingStarted = new GettingStarted(this.senderId);
      gettingStarted.sendAction();
      break;
    default:

  }
}

module.exports = UserResponse;
