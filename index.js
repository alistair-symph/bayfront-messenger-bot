'use strict';

// middlewares
var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');

// local modules
var ContactUs = require('./actions/contact-us.js');
var GettingStarted = require('./actions/getting-started.js');
var RoomDetails = require('./actions/room-details.js');
var RoomRates = require('./actions/room-rates.js');
var SuiteRoomDetails = require('./actions/suite-details.js');
var WheresTheHotel = require('./actions/where-hotel.js');
var FunctionRoom = require('./actions/function-rooms.js');
var HotelInfo = require('./actions/hotel-info.js');
var ConferenceRoom = require('./actions/conference-room.js');
var Promo = require('./actions/promos.js');
var UserResponse = require('./user-response');

var app = express();

app.set('port', (process.env.PORT || 5000));
app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello Vorld! Hello Node!');
  res.status(200);
});

app.listen(app.get('port'), () => {
  console.log('running on port', app.get('port'));
});

app.get('/webhook/', (req, res) => {
  if (req.query['hub.mode'] === 'subscribe' &&
  req.query['hub.verify_token'] === 'NTk0NTdhYzhkMjljNWVjNTFlNmU5ZjYx') {
    console.log('Validating webhook...')
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error('Error! Wrong verification token used.');
    res.status(403);
  }
});

app.post('/webhook', (req, res) => {
  var data = req.body;
  // Make sure that this page is a subscription
  if (data.object === 'page') {
    // iterate over each entry - there may be multiple if batched
    data.entry.forEach((pageEntry) => {
      var pageId = pageEntry.id;
      var timeOfEvent = pageEntry.time;

      // iterate over each messaging event
      pageEntry.messaging.forEach((event) => {
        if (event.optin) {
          console.log(event);
        } else if (event.message) {
          receivedMessage(event);
        } else if (event.postback) {
          receivedPostback(event);
        } else {
          console.log("Webhook received unknown event: ", event);
        }
      });
    });
  }
  // if all goes well, we should send back 200 to avoid retries
  res.sendStatus(200);
});

function receivedMessage(event) {
  console.log("Message data: ", JSON.stringify(event.message));
  // todo analyses user's message
  var userResponse = new UserResponse(event);
  userResponse.listen();
}

function receivedPostback(event) {
  var senderId = event.sender.id;
  var recipientId = event.recipient.id;
  var timestamp = event.timestamp;

  var payload = event.postback.payload;

  switch (payload) {
    case 'PAYLOAD_GET_STARTED':
      var gettingStarted = new GettingStarted(senderId);
      gettingStarted.sendAction();
      break;

    case 'PAYLOAD_MENU_HOTEL_INFO':
      var hotelInfo = new HotelInfo(senderId);
      hotelInfo.sendAction();
      break;

    case 'PAYLOAD_MENU_ROOMS_RATES':
      var roomRates = new RoomRates(senderId);
      roomRates.sendAction();
      break;

    case 'PAYLOAD_MENU_LOCATION':
      break;

    case 'PAYLOAD_MENU_ROOMS':
      var menuRoomRates = new RoomRates(senderId);
      menuRoomRates.sendAction();
      break;

    case 'PAYLOAD_MENU_CSR':
      var contactUs = new ContactUs(senderId);
      contactUs.sendAction();
      break;

    case 'PAYLOAD_ROOM_DETAILS':
      var roomDetails = new RoomDetails(senderId);
      roomDetails.sendAction();
      break;

    case 'PAYLOAD_SUITE_ROOM_DETAILS':
      var suiteRoomDetails = new SuiteRoomDetails(senderId);
      suiteRoomDetails.sendAction();
      break;

    case 'PAYLOAD_WHERES_HOTEL':
      var wheresHotel = new WheresTheHotel(senderId);
      wheresHotel.sendAction();
      break;

    case 'PAYLOAD_FUNCTION_ROOMS':
      var functionRoom = new FunctionRoom(senderId);
      functionRoom.sendAction();
      break;

    case 'PAYLOAD_PROMOS_PACKAGES':
      var promos = new Promo(senderId);
      promos.sendAction();
      break;

    case 'PAYLOAD_CONFERENCE_ROOM':
      var conferenceRoom = new ConferenceRoom(senderId);
      conferenceRoom.sendAction();
      break;

    default:
      console.log('payload not assigned: ', payload);
  }

  console.log("Received postback for user %d and page %d with payload '%s' " +
    "at %d", senderId, recipientId, payload, timestamp);
}

module.exports = app;
