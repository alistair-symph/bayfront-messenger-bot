'use strict'

/**
 * One stop shop where all your constants are defined. You never need to
 * change these constants in each of every JS file
 */
module.exports = {
  BASE_URL: 'https://graph.facebook.com/v2.8',
  MESSAGE_API: '/me/messages',
  THREAD_API: '/me/thread_settings'
};
