'use strict';

var request = require('request');
var Const = require('../constant.js');

function RoomRates(recipientId) {
  this.recipientId = recipientId;

  this.sendAction = () => {
    var messageData = {
      "attachment" : {
        "type" : "template",
        "payload" : {
          "template_type" : "generic",
          "elements" : [
            {
              "title" : "Standard Single",
              "subtitle" : "PHP 855 per night (without breakfast)",
              "image_url" : "http://thecentersuites.com/wp-content/uploads/2013/10/001Single-x.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "Room Details",
                  "payload" : "PAYLOAD_ROOM_DETAILS"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            },


            {
              "title" : "Deluxe Single",
              "subtitle" : "PHP 985 per night (without breakfast)",
              "image_url" : "http://thecentersuites.com/wp-content/uploads/2013/10/001Single-x.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "Room Details",
                  "payload" : "PAYLOAD_ROOM_DETAILS"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            },


            {
              "title" : "Twin Sharing",
              "subtitle" : "PHP 1,299 per night (without breakfast)",
              "image_url" : "http://thecentersuites.com/wp-content/uploads/2013/10/002TwinSharing-x.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "Room Details",
                  "payload" : "PAYLOAD_ROOM_DETAILS"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            },


            {
              "title" : "Deluxe Matrimonial",
              "subtitle" : "PHP 1,299 per night (without breakfast)",
              "image_url" : "http://thecentersuites.com/wp-content/uploads/2013/10/003dmatrimonial-x.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "Room Details",
                  "payload" : "PAYLOAD_ROOM_DETAILS"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            },


            {
              "title" : "Standard Family Room",
              "subtitle" : "PHP 1,795 per night (without breakfast)",
              "image_url" : "http://thecentersuites.com/wp-content/uploads/2013/10/004family-x.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "Room Details",
                  "payload" : "PAYLOAD_ROOM_DETAILS"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            },


            {
              "title" : "Deluxe Family Room for 4",
              "subtitle" : "PHP 2,399 per night (without breakfast)",
              "image_url" : "http://thecentersuites.com/wp-content/uploads/2013/10/005dfamily-x.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "Room Details",
                  "payload" : "PAYLOAD_ROOM_DETAILS"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            },


            {
              "title" : "Suite Room for 2",
              "subtitle" : "PHP 1,595 and PHP 1,699 per night (both without breakfast)",
              "image_url" : "http://thecentersuites.com/wp-content/uploads/2013/10/007suite2-x.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "Room Details",
                  "payload" : "PAYLOAD_SUITE_ROOM_DETAILS"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            },


            {
              "title" : "Suite Room for 3",
              "subtitle" : "PHP 2,550 per night (without breakfast)",
              "image_url" : "http://thecentersuites.com/wp-content/uploads/2013/10/008suite3-x.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "Room Details",
                  "payload" : "PAYLOAD_SUITE_ROOM_DETAILS"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            }


          ]
        }
      }
    }
    request({
      url : Const.BASE_URL + Const.MESSAGE_API,
      qs : { access_token : process.env.FB_ACCESS_TOKEN },
      method : 'POST',
      json : {
        recipient : { id : this.recipientId },
        message : messageData
      }
    }, (error, response, body) => {
      if (error) {
        console.log("Error sending messages:", error);
      } else if (response.body.error) {
        console.error("Error:", response.body.error);
      }
    });
  };
}

module.exports = RoomRates;
