'use strict'

var request = require('request');
var Const = require('../constant.js');

function HotelInfo(recipientId) {
  this.recipientId = recipientId;

  this.sendAction = () => {
    var messageData = {
      "attachment" : {
        "type" : "template",
        "payload" : {
          "template_type" : "generic",
          "elements" : [
            {
              "title" : "Meetings & Events",
              "subtitle" : "A 120-square meter useable function area that provides the perfect accommodation for warm and intimate gatherings.",
              "image_url" : "http://i.imgur.com/r1sBa7n.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "View Function Rooms",
                  "payload" : "PAYLOAD_FUNCTION_ROOMS"
                }
              ]
            },
            {
              "title" : "Promos & Packages",
              "subtitle" : "Checkout our latest promos and discounts this month of December",
              "image_url" : "https://i.imgur.com/oThAzwG.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "View Details",
                  "payload" : "PAYLOAD_PROMOS_PACKAGES"
                }
              ]
            }
          ]
        }
      }
    }
    request({
      url : Const.BASE_URL + Const.MESSAGE_API,
      qs : { access_token : process.env.FB_ACCESS_TOKEN },
      method : 'POST',
      json : {
        recipient : { id : this.recipientId },
        message : messageData
      }
    }, (error, response, body) => {
      if (error) {
        console.log("Error sending messages:", error);
      } else if (response.body.error) {
        console.error("Error:", response.body.error);
      }
    });
  };
}

module.exports = HotelInfo;
