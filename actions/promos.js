'use strict';

var request = require('request');
var Const = require('../constant.js');

function Promo(recipientId) {
  this.recipientId = recipientId;

  this.sendAction = () => {
    var messageData = {
      text : "We have exciting promos coming this month! Stay tuned."
    }
    request({
      url : Const.BASE_URL + Const.MESSAGE_API,
      qs : { access_token : process.env.FB_ACCESS_TOKEN },
      method : 'POST',
      json : {
        recipient : { id : this.recipientId },
        message : messageData
      }
    }, (error, response, body) => {
      if (error) {
        console.log("Error sending messages:", error);
      } else if (response.body.error) {
        console.error("Error:", response.body.error);
      }
    });
  };
}

module.exports = Promo;
