'use strict';

var request = require('request');

function ContactUs(recipientId) {
  this.recipientId = recipientId;

  this.sendAction = () => {
    var messageData = {
      "attachment" : {
        "type" : "template",
        "payload" : {
          "template_type" : "button",
          "text" : "Contact us through these lines.",
          "buttons" : [
            {
              "type" : "phone_number",
              "title" : "Globe",
              "payload" : "+639062744939"
            },
            {
              "type" : "phone_number",
              "title" : "Smart/Sun",
              "payload" : "+639256282016"
            },
            {
              "type" : "phone_number",
              "title" : "Landline",
              "payload" : "+63322668885"
            }
          ]
        }
      }
    }
    request({
      url : 'https://graph.facebook.com/v2.8/me/messages',
      qs : { access_token : process.env.FB_ACCESS_TOKEN },
      method : 'POST',
      json : {
        recipient : { id : this.recipientId },
        message : messageData
      }
    }, (error, response, body) => {
      if (error) {
        console.log("Error sending messages:", error);
      } else if (response.body.error) {
        console.error("Error:", response.body.error);
      }
    });
  }

}

module.exports = ContactUs;
