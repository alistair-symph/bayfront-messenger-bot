'use strict';

var request = require('request');
var Const = require('../constant.js');

function ConferenceRoom(recipientId) {
  this.recipientId = recipientId;

  this.sendAction = () => {
    var messageData = {
      text : "Conference Room 1 is a stylish venue to hold your important meetings, presentations and social events.\n\nSeating up to 800 dinner guests and accommodating up to 1,200 for cocktails, the Center Suites Grand Ballroom provides a most impressive venue for functions that truly leave a mark."
    }
    request({
      url : Const.BASE_URL + Const.MESSAGE_API,
      qs : { access_token : process.env.FB_ACCESS_TOKEN },
      method : 'POST',
      json : {
        recipient : { id : this.recipientId },
        message : messageData
      }
    }, (error, response, body) => {
      if (error) {
        console.log("Error sending messages:", error);
      } else if (response.body.error) {
        console.error("Error:", response.body.error);
      }
    });
  };
}

module.exports = ConferenceRoom;
