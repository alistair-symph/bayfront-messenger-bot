'use strict';

var request = require('request');
var Const = require('../constant.js');

function GettingStarted(recipientId) {
  this.recipientId = recipientId;

  this.sendAction = () => {
    var messageData = {
      "attachment" : {
        "type" : "template",
        "payload" : {
          "template_type" : "generic",
          "elements" : [
            {
              "title" : "Hi there!",
              "subtitle" : "Welcome to The Center Suites. Please select an option.",
              "image_url" : "http://thecentersuites.com/wp-content/uploads/2012/06/TCS17.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "Hotel Info",
                  "payload" : "PAYLOAD_MENU_HOTEL_INFO"
                },
                {
                  "type" : "postback",
                  "title" : "Room Rates",
                  "payload" : "PAYLOAD_MENU_ROOMS_RATES"
                },
                {
                  "type" : "postback",
                  "title" : "Location",
                  "payload" : "PAYLOAD_MENU_LOCATION"
                }
              ]
            }
          ]
        }
      }
    }
    request({
      url : Const.BASE_URL + Const.MESSAGE_API,
      qs : { access_token : process.env.FB_ACCESS_TOKEN },
      method : 'POST',
      json : {
        recipient : { id : this.recipientId },
        message : messageData
      }
    }, (error, response, body) => {
      if (error) {
        console.log("Error sending messages:", error);
      } else if (response.body.error) {
        console.error("Error:", response.body.error);
      }
    });
  }
}

module.exports = GettingStarted;
