'use strict';

var request = require('request');
var Const = require('../constant.js');

function FunctionRoom(recipientId) {
  this.recipientId = recipientId;

  this.sendAction = () => {
    var messageData = {
      "attachment" : {
        "type" : "template",
        "payload" : {
          "template_type" : "generic",
          "elements" : [
            {
              "title": "Conference Room 1",
              "subtitle" : "A stylish venue to hold your important meetings, presentations and social events.",
              "image_url" : "http://i.imgur.com/BOuznjx.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "View Details",
                  "payload" : "PAYLOAD_CONFERENCE_ROOM"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            },
            {
              "title": "Function Room 1",
              "subtitle" : "A simple classroom set up that can accommodate approximately 50 seats.",
              "image_url" : "http://i.imgur.com/dFKhC3p.jpg",
              "buttons" : [
                {
                  "type" : "postback",
                  "title" : "View Details",
                  "payload" : "PAYLOAD_CONFERENCE_ROOM"
                },
                {
                  "type" : "phone_number",
                  "title" : "Call Now To Book",
                  "payload" : "+639256282016"
                }
              ]
            }
          ]
        }
      }
    }
    request({
      url : Const.BASE_URL + Const.MESSAGE_API,
      qs : { access_token : process.env.FB_ACCESS_TOKEN },
      method : 'POST',
      json : {
        recipient : { id : this.recipientId },
        message : messageData
      }
    }, (error, response, body) => {
      if (error) {
        console.log("Error sending messages:", error);
      } else if (response.body.error) {
        console.error("Error:", response.body.error);
      }
    });
  };
}

module.exports = FunctionRoom;
