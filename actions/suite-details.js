'use strict';

var request = require('request');
var Const = require('../constant.js');

function SuiteRoomDetails(recipientId) {
  this.recipientId = recipientId;

  this.sendAction = () => {
    var messageData = {
      text : "Suite rooms are complete with:\n\n1. air-conditioner\n2. flat screen LED television\n3. a telephone\n4. a side table drawer with lock\n5. study desk and chair\n6. free wifi\n7. a sofa\n8. mini refrigerator\n9. and a safety deposit box\n\nEach room has its own individual bathroom with hot and cold shower."
    }
    request({
      url : Const.BASE_URL + Const.MESSAGE_API,
      qs : { access_token : process.env.FB_ACCESS_TOKEN },
      method : 'POST',
      json : {
        recipient : { id : this.recipientId },
        message : messageData
      }
    }, (error, response, body) => {
      if (error) {
        console.log("Error sending messages:", error);
      } else if (response.body.error) {
        console.error("Error:", response.body.error);
      }
    });
  };

}

module.exports = SuiteRoomDetails;
